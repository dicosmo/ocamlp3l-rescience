(* compute a function over a stream of floats using a farm  *)
(* very simple code to test the speed-up of a farm skeleton *)

let msecwait = 100;; (* time spent in sequential computation, microseconds *)
let nproc = 1;;      (* number of nodes allocated in the farm skeleton *)

(* active wait for n microseconds *)
let microwait n =
  let t = Unix.gettimeofday() in
  let now = ref (Unix.gettimeofday()) in
  while(!now < (t +. (0.001 *. (float n)))) do
    now := Unix.gettimeofday()
  done;;

let farm_worker comptime =
  (function x ->
  (microwait comptime ; x *. x));;

let nothing _ = ();;

let generate_input_stream =
  let x = ref 0.0 in
  (function () ->
    begin
      x := !x +. 1.0;
      if(!x < 129.0) then !x else raise End_of_file
    end);;

(* the timing stuff *)
let now = ref (Unix.gettimeofday());;
let stop_init _ =
  now := (Unix.gettimeofday());;
let stop_end _ =
  print_string "Elapsed time on stop node is ";
  print_float ((Unix.gettimeofday()) -. !now );
  print_newline();;

let do_nothing _ = ();;

let progr ()=
  startstop
    (generate_input_stream, nothing)
    (do_nothing,stop_init,stop_end)
    (farm(seq(farm_worker msecwait), nproc))
in pardo progr;;
