\newcommand{\OCamlPiiil}{{\texttt{OCamlP3l}}}

This article provides a full report on the effort to reproduce the work
described in the article \emph{``Parallel Functional Programming with Skeletons: the OCamlP3L experiment''}~\cite{danelutto:hal-01499962}, written in 1998. It presented \OCamlPiiil{}~\cite{ocamlp3l}, a parallel programming system written in the OCaml
programming language~\cite{leroy:hal-00930213}.\\

The system described in~\cite{danelutto:hal-01499962} was a breakthrough in
many respects: it showed that it was possible to implement \emph{parallel
skeletons}~\cite{ColeSkeletons} a \emph{combinators} in a functional
programming language; it showed how this parallel programming style allowed to
\emph{write a single source code} that produced executables targeted for sequential
execution, hence enabling usual debugging techniques, and executables for
parallel execution; and it led to the introduction in OCaml of the ability to
\emph{marshal functional closures}, used later on by a wealth of different applications.\\

The article consists of two main parts, the system description, and the system
evaluation, so replicating the results involves the following:

\begin{enumerate}
  \item recover the source code of the \OCamlPiiil{} system
  \item make it compile and run on a modern OCaml 4.x system
  \item recover the tests used in the system evaluation
  \item verify we can get speedup in performance similar to the one reported in the article.\\
\end{enumerate}

When starting this replication effort, we had the following expectations:

\begin{enumerate}
  \item recover the source code should be \emph{easy}: just look in the paper directory on our machines
  \item compile and run might be \emph{difficult}: the code was designed 23 years ago for OCaml 1.07 
  \item recover tests should be \emph{easy}: just look in the paper directory on our machines
  \item verify speedup might be \emph{challenging}: many parameters may have changed in microprocessors and network.\\
\end{enumerate}

The reality turned out to be surprisingly different. In the following we sum up
the steps that we performed to address each of these four challenges, and the
final outcome.

\section{Recovering the source code}
Looking into the original paper directory turned out to be of little help, as
there was no trace of the source code or any useful information. So we turned
to the paper itself, and found three links to web pages:

\begin{itemize}
   \item \url{www.di.unipi.it/~marcod/ocamlp3l/ocamlp3l.ml}, that today returns \texttt{404}; looking at the archived copies on the \url{archive.org} allowed to recover some documentation, but not the source code;
   \item \url{www.di.unipi.it/~susanna/p3l.ml}, that is still live, but provides no useful link to the source code
   \item \url{pauillac.inria.fr/ocaml}, that is also live, but the only hope to find the source code was
         the link to the \emph{anonymous CVS server} which points today to the OCaml GitHub organization, where
         we found no trace of this 23 years old code.
\end{itemize}

\paragraph{Searching the web}
The links from the original paper being now useless, we resorted to searching
the web, and found \url{http://ocamlp3l.inria.fr/}. We followed the
link \url{http://ocamlp3l.inria.fr/eng.htm#download} to the download page that
offered an ftp
link, \url{ftp://ftp.inria.fr/INRIA/caml-light/bazar-ocaml/ocamlp3l/ocamlp3l-2.03.tgz},
now dead, and web link, \url{http://ocamlp3l.inria.fr/ocamlp3l-2.03.tgz} that
was still working. Unfortunately, this is version 2.03 of \OCamlPiiil{}, way
more evolved, and quite different from the version 0.9 used in the original
research article, and there was no trace of the version history, so the quest
was far from over.

\paragraph{Saving version 2.03}
Here we decided to make a pause, and properly deposit this version 2.03, with
extended metadata, into Software Heritage~\cite{SwhCACM2018} via the HAL
national open access archive, the result being now availabe as~\cite{OCamlP3l2.03}.

\paragraph{Back to searching the web}
More web searches brough up a related webpage for a newer
system, \url{http://camlp3l.inria.fr/eng.htm} touting a link to a git repository
on Gitorious, \url{http://gitorious.org/camlp3l/}. Unfortunately, following the
link leads to nowhere, as Gitorious has been shutdown in 2015, but luckily Software
Heritage has saved the full content of Gitorious, so we could download a 
\href{https://archive.softwareheritage.org/browse/origin/https://gitorious.org/camlp3l/camlp3l.git/directory/}{a full copy of the git repository}, but unfortunately its version history only goes back to 2011,
with version 1.03 of CamlP3l, not \OCamlPiiil{}, and no trace of earlier versions of
the system, so we were seemingly back to square one.

\paragraph{Finding it on Software Heritage}
This long journey gave us an idea: what about searching directly in Software Heritage?
This turned out to be the lucky strike: a full copy of \href{https://archive.softwareheritage.org/browse/origin/https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git/directory/}{https://gitorious.org/ocamlp3l/ocamlp3l\_cvs.git} had been safely archived by Software Heritage in 2015, and we found in it the whole version history starting from 1997.
The journey ended successfully, \emph{we had found the source code}!

\section{Compiling and running}
We did not know exactly which version of the source code was used in the
article, but since the article was published in September 1998, it seemed safe
to pick in the version control system a stable version dating from a few
months before.\\

We settled for the version of code source available in the directory whose
SWH-ID is \href{https://archive.softwareheritage.org/swh:1:dir:01d2169c88d0783182b1b7facffa522ba09b5957;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git/;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0}{swh:1:dir:01d2169c88d0783182b1b7facffa522ba09b5957}
contained in the revision dated June 23rd 1991 with
SWH-ID \href{https://archive.softwareheritage.org/swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git/}{swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0}.\\

The code contained in this directory seems to be version 1.0~\cite{ocamlp3l:1-0}
and is classified as follows by the \texttt{sloccount} utility:\\

\begin{center}
\begin{tabular}{|r|l|l|}
\hline
SLOC &   Directory          & SLOC-by-Language (Sorted)\\\hline
1490 &   ocamlp3l-vprocess  & ml=1490\\
1451 &   Source             & ml=1451\\
1162 &   Examples           & ml=1138,perl=13,csh=11\\
159  &   ocamlp3l-vthread   & ml=159\\
67   &   Doc                & ml=67\\
31   &   Tools              & csh=31\\\hline
\end{tabular}
\end{center}

To our great surprise, and satisfaction, the code compiled with the modern OCaml
4.05 installed on our machines \emph{unchanged}. The only notable difference is
that the modern compiler produces several new warnings that correspond to better
static analysis checks introduced over the past \emph{quarter of a century}.\\

This is a remarkable achievement, not just for our own code, but for OCaml itself.

\section{Recovering the test suite and replicating speedup figures}
Here too, looking into the original paper directory turned out to be of little
help, as there was no trace of the test suite used in the article or any useful
information. Web searches were of little interest, as this test suite was used
only for the article and not published. A long search through old backups on
tape, CR-ROMS and DVDs did not yield anything relevant either.
Hence, our \emph{reproducibility} journey ended here.\\

But we did not want to stop here: having found the original code, we could
\emph{replicate} the speed-up results, using \emph{a new test suite}. After all,
according to the article we wrote over 22 years ago, the original test suite was
just producing a computational load to keep the compute nodes busy enough to
take advantage of the parallelism.\\

As a first step, we adapted code in the Examples directory, from the
\href{https://archive.softwareheritage.org/swh:1:cnt:4d99d2d18326621ccdd70f5ea66c2e2ac236ad8b;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git/;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;path=/Examples/SimpleFarm/simplefarm.ml}{SimpleFarm/simplefarm.ml}~\cite{simplefarm}
and the
\href{https://archive.softwareheritage.org/swh:1:cnt:8415f9451cf1ecaef70daab45c0ea2e5200f7d38;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;path=/Examples/PerfTuning/pipeline.ml}{PerfTuning/pipeline.ml}~\cite{pipeline}
files. The result is a simple parametric test code, shown in
Figure~\ref{fig:testspeedup}, that allows to test the speedup one can get
from the \texttt{farm} parallel skeleton in configurations obtained by varying
the number \texttt{nproc} of processing nodes, and the time \texttt{msecwait}
elapsed in each sequential computation.\\


The second step was to make the \href{https://archive.softwareheritage.org/swh:1:cnt:c428f4deb1cdff8500fff5c449b99454a816c163;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;path=/Tools/ocamlp3lrun}{\texttt{ocamlp3lrun}} driver command~\cite{ocamlp3lrun}, that was
using \texttt{rsh} (see \href{https://archive.softwareheritage.org/swh:1:cnt:c428f4deb1cdff8500fff5c449b99454a816c163;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git;lines=105-113/;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;path=/Tools/ocamlp3lrun}{these two occurrences}) and \texttt{rcp} (see \href{https://archive.softwareheritage.org/swh:1:cnt:c428f4deb1cdff8500fff5c449b99454a816c163;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git;lines=89/;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;path=/Tools/ocamlp3lrun}{this occurrence}) back in 1997 , work with the \texttt{ssh} and
\texttt{scp} commands that are mainstream today.\\
A quick hack that works without even touching the code is to create an
executable file \texttt{rsh} containing just the two lines:

\lstset{numbers=none, frame=none, language=bash}
\begin{lstlisting}
  #!
  ssh $*
\end{lstlisting}
\mbox{}\\
and similarly for \texttt{rcp}. Running the parallel test on a set of $n$ different machines is then a simple matter of issueing the commands
%
\lstset{numbers=none, frame=none, language=bash}
\begin{lstlisting}
  ocamlp3lcc -par test-for-speedup.ml
  ocamlp3lrun test-for-speedup <machine1> <machine2> ... <machinen>
\end{lstlisting}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{ %
  language=[Objective]Caml,                % the language of the code
  basicstyle=\footnotesize,           % the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  numberstyle=\footnotesize,          % the size of the fonts that are used for the line-numbers
%  stepnumber=2,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
  belowskip=0pt,
%  backgroundcolor=\color{white},      % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                   % adds a frame around the code
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                   % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
%  numberstyle=\tiny\color{gray},        % line number style
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\color{dkgreen},       % comment style
  stringstyle=\color{mauve},         % string literal style
%  escapeinside={(*}{*)},            % if you want to add a comment within your code
  morekeywords={*,...}               % if you want to add more keywords to the set
}

\begin{figure}[h!]
  \lstinputlisting{test-for-speedup.ml}
\caption{Test code for evaluating speedup of a farm skeleton, by varying the \texttt{nproc} and \texttt{msecwait} parameters. The exact source of the test suite has SWH-ID \href{https://archive.softwareheritage.org/swh:1:cnt:8e7f96cb82d50ea73c2d8e4bf2c832b0ada49a7e}{swh:1:cnt:8e7f96cb82d50ea73c2d8e4bf2c832b0ada49a7e}}\label{fig:testspeedup}
\end{figure}

The third step was to run a parameter sweep experiment on a cluster available
at the University of Pisa, and collect the data that was used to
produce the new figures that we show in Figure~\ref{fig:speedup}.\\

The cluster is configured with 32 nodes each equipped with dual socket Intel(R)
Xeon(R) CPU E5-2640 v4 \@2.40GHz. At the time of this experiment 5 nodes where
busy or in maintainance and therefore our replication experiments were run with
parallelism degrees $n_w \in [1-24]$. It is worth pointing out that the cluster
nodes, differently from the ones used in the original experiments, sport 20
cores with 2-way hyperthreading. Hence, in order to replicate the very same
experiments dating back to late '90s, we used only one process per node, as if
the node had a single processor available.

%\clearpage

\begin{figure}[th]
\begin{subfigure}{.5\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{tc-crop.pdf}  
  \caption{Completion time for three different stream sizes (128, 256 and 512 items)}
  \label{fig:tc}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{sp-crop.pdf}  
  \caption{ Measured vs. ideal speedup for the three different stream sizes (128, 256 and 512 items)}
  \label{fig:sp}
\end{subfigure}

% \newline

\begin{subfigure}{.5\textwidth}
  \centering
  % include second image
  \includegraphics[width=.8\linewidth]{stab-crop.pdf}  
  \caption{Stability: Completion times measured in three different consecutive experiments}
  \label{fig:stab}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include second image
  \includegraphics[width=.8\linewidth]{gus-crop.pdf}  
\caption{Scaled speedup: stream size proportional to the parallelism degree}
  \label{fig:gus}
\end{subfigure}
  \caption{Replication results. Syntetic benchmark using a farm pattern.}
\label{fig:speedup}
\end{figure}

Figure~\ref{fig:tc} and Figure~\ref{fig:sp} show the completion times and the
relative speedups measured in three different experiments, processing streams of
data items of different lengths. The completion times are very close to the
ideal ones, but looking at the speedup figures we can see that the larger the
load the better speedup is achieved. Indeed, the inter-arrival time of tasks on
the stream is negligible with respect to the time spent processing the single
task and therefore longer streams help giving more work to each one of the
``workers'' in the parallel farm. This, in turn, results in a minor impact of
the overheads associated to the set up and orchestration of the nodes that
take part in the computation.
%
Figure~\ref{fig:stab} shows the result of three different runs of the same
experiments, executed at three different times of the same day on the cluster.
We observe the same completion times, confirming the stability results already
achieved at the time OcamlP3L was developed.
%
Finally, Figure~\ref{fig:gus} reports the scaled speedup results. For each
parallelism degree $n_w$, we used an input stream whose length was $k \times
n_w$. The measured completion times are almost constants and close to the ideal
one, which is the sequential time taken to compute a $k$ item stream.\\

To sum up, we could replicate quite faithfully the quality of the results
achieved more than 20 years ago. It is worth pointing out that this is a
nontrivial achievement, as the architectures used for these experiments today
and in the past are completely different, both in terms of computation power
(processors) and in terms of communication bandwidth and latency (network
interface cards).

In our opinion, this is clearly due to two distinct and synergic factors:
\begin{itemize}
\item the clean ``functional'' design and implementation of OcamlP3l,
  that resisted to language and system development, and
\item the algorithmic skeleton\footnote{aka ``parallel design
  patterns''} principles which are the base of the overall
  implementation of OcamlP3L that naturally implement ``portability''
  across different architectures, independently of the fact the
  architectures use different hardware.
\end{itemize}

\section{Conclusion}

We have reported on our experience in reproducing work we have done ourselves on
the \OCamlPiiil{} experiment over 22 years
ago~\cite{danelutto:hal-01499962}. Contrary to our expectations, the most
difficult part has been to \emph{recover the source code}. For its presevation,
we had relied on institutional repositories first, and freely available
collaborative development platforms later, neither of which passed the test of
time.\\

We are delighted to report that leveraging the Software Heritage
archive~\cite{SwhCACM2018} we have been able to recover the full history of
development of the system, and rebuild it as it likely was at the time the
original article had been published. Despite the fact that we did not find
the exact test suite used 22 years ago to test the scalability of the system,
we have been able to \emph{replicate the results} on modern hardware.\\

As a byproduct of this work, we have also safely archived in Software Heritage,
and described in HAL, the stable final release 2.3 of
\OCamlPiiil{}~\cite{OCamlP3l2.03}.\\

Based on this experience, we strongly suggest to systematically archive and reference
research source code following the Software Heritage guidelines~\cite{dicosmo:hal-02263344}.






